import numpy as np
import cv2 as cv
import random
import configparser
import os
import glob
import utility
import csv
import time

from BackgroundFileInterface  import BackgroundFileLoader
from SampleImgInterface import SampImgModifier

DEFAULT_PARAMS={
'BackgroundFilePath':'./Data/background',
'SampleFilesPath':'./Data/GermanFlag',
'bgColor': 255,
'bgTthresh':8,
'maxXangle':50,
'maxYangle':50,
'maxZangle':50,
'maxAngle_Affine':30,
'outputPerSample':300,
'GausNoiseProb':0.2,
'MedianNoiseProb':0.1,
'AffineRotateProb':0.3,
'SharpenProb':0.2,
'PerspTransProb':0.8,
'ScalingProb':0.7,
'BrightnessProb':1,
'OutputPath':'./Data/GermanFlag/Default',
'outputPerSample':100
}

def placeDistortedSample(outImgTight,foregroundPixTight,BoundRect,bkgImg):

    bgHeight, bgWidth, _ = np.shape(bkgImg)
    outHeight,outWidth,_ = np.shape(outImgTight)

    if (outHeight <  bgHeight and outWidth <bgWidth):

        finalImg=np.array(bkgImg).copy()

        posX = np.random.randint(0,bgWidth-outWidth)
        if (posX + outWidth > bgWidth):
            posX = bgWidth - outWidth - 10

        posY = np.random.randint(0,bgHeight-10)
        if (posY + outHeight > bgHeight-outHeight):
            posY = bgHeight - outHeight - 10

        indices=np.zeros((np.shape(foregroundPixTight)),np.uint64)
        indices[0] = np.array([foregroundPixTight[0]]) + posY
        indices[1] = np.array([foregroundPixTight[1]]) + posX

        boundRectFin =np.zeros((2,2),float)
        #The order of x and y have been reversed for yolo
        boundRectFin[1][1] = float(BoundRect[1][0]-BoundRect[0][0] )/float(bgHeight)
        boundRectFin[1][0] = float(BoundRect[1][1] - BoundRect[0][1])/float(bgWidth)
        boundRectFin[0][1] = float(posY)/float(bgHeight)+boundRectFin[1][1]/float(2)
        boundRectFin[0][0] = float(posX)/float(bgWidth)+boundRectFin[1][0]/float(2)


        foregroundpixBkg = tuple(map(tuple, indices))
        finalImg[foregroundpixBkg] = outImgTight[foregroundPixTight]
        return True,finalImg,boundRectFin
    else:
        return False,0,0

def clusterMask(objectMask):
    criteria = (cv.TERM_CRITERIA_EPS + cv.TERM_CRITERIA_MAX_ITER, 10, 1.0)
    ret,label,center=cv.kmeans(np.float32(objectMask),2,None,criteria,10,cv.KMEANS_RANDOM_CENTERS)
    cluster1 = label==0
    cluster2 = label==1
    
    objectLabel=None
    if(objectMask[center[0]]==[255,255,255]):#Comprabar si es el label del objeto comparando si el centro están en un color blanco
        objectLabel=center[0]
    else:
        objectLabel=center[1]
    
    return cv.bitwise_and()

    
def cropImage(image, objectMask, threshold):   
    canny_output = cv.Canny(objectMask, threshold, threshold * 3)
    cv.imshow('im', canny_output)
    contours, hierarchy  = cv.findContours(canny_output, cv.RETR_TREE, cv.CHAIN_APPROX_SIMPLE)
    
    max_bounding_box = (None,None,None,None)
    max_bb_area = 0
    contours_poly=[None]*len(contours)
    boundRect = [None]*len(contours)
    centers = [None]*len(contours)
    radius = [None]*len(contours)
    for i, c in enumerate(contours):
        contours_poly[i] = cv.approxPolyDP(c, 3, True)
        boundRect[i] = cv.boundingRect(contours_poly[i])
        bb_area = boundRect[i][2]*boundRect[i][3]
        if(bb_area > max_bb_area):
            max_bb_area = bb_area
            max_bounding_box = boundRect[i]
        
        #centers[i], radius[i] = cv.minEnclosingCircle(contours_poly[i])
    
    #drawing = np.zeros((canny_output.shape[0], canny_output.shape[1], 3), dtype=np.uint8)
    print(max_bounding_box)
    """
    for i in range(len(contours)):
        color = (100,100,100)#(rng.randint(0,256), rng.randint(0,256), rng.randint(0,256))
        cv.drawContours(drawing, contours_poly, i, color)
        cv.rectangle(drawing, (int(boundRect[i][0]), int(boundRect[i][1])), \
          (int(boundRect[i][0]+boundRect[i][2]), int(boundRect[i][1]+boundRect[i][3])), color, 2)
        cv.circle(drawing, (int(centers[i][0]), int(centers[i][1])), int(radius[i]), color, 2)
    """
    
    x1=max_bounding_box[0]
    y1=max_bounding_box[1]
    x2=x1+max_bounding_box[2]
    y2=y1+max_bounding_box[3]
    
    cropped = image[y1:y2,x1:x2]
    #cv.imshow('Image', cropped)
    return cropped
    

def main():

    parser=configparser.RawConfigParser(defaults=DEFAULT_PARAMS)
    parser.read('Parameters.config')

    backgroundFilePath=parser.get('USER_PARAMS','backgroundFilePath')
    samplePath = parser.get('USER_PARAMS', 'sampleFilesPath')
    outputfolder =parser.get('USER_PARAMS', 'OutputPath')
    bgColor = int(parser.get('USER_PARAMS','bgColor'))
    bgThresh = int(parser.get('USER_PARAMS','bgThresh'))
    maxXangle_Persp = int(parser.get('USER_PARAMS', 'maxXangle'))
    maxYangle_Persp = int(parser.get('USER_PARAMS', 'maxYangle'))
    maxZangle_Persp = int(parser.get('USER_PARAMS', 'maxZangle'))
    maxAngle_Affine = int (parser.get('USER_PARAMS','maxAngle_Affine'))
    GaussianNoiseProb= float(parser.get('USER_PARAMS', 'GausNoiseProb'))
    MedianNoiseProb=float(parser.get('USER_PARAMS', 'MedianNoiseProb'))
    SharpenProb=float(parser.get('USER_PARAMS', 'SharpenProb'))
    PerspTransProb = float(parser.get('USER_PARAMS', 'PerspTransProb'))
    ScalingProb = float(parser.get('USER_PARAMS', 'ScalingProb'))
    BrightnesProb=float(parser.get('USER_PARAMS', 'BrightnessProb'))
    outputPerSample = float(parser.get('USER_PARAMS', 'outputPerSample'))
    AffineRotateProb=float(parser.get('USER_PARAMS', 'AffineRotateProb'))
    CannyLowThreshold = int (parser.get('USER_PARAMS','CannyLowThreshold'))
    
    Alpha2WhitePNG=bool(parser.get('USER_PARAMS', 'Alpha2WhitePNG'))

    if not(os.path.isdir(outputfolder)):
        os.makedirs(outputfolder)

    bkgFileLoader=BackgroundFileLoader()
    bkgFileLoader.loadbkgFiles(backgroundFilePath)
    
    extensionFile = '*.jpg'
    if Alpha2WhitePNG:
        extensionFile = '*.png'
    
    for sampleImgPath in glob.glob(os.path.join(samplePath, extensionFile)):

        filenameWithExt=os.path.split(sampleImgPath)[1]
        filename=os.path.splitext(filenameWithExt)[0]

        sampleImg=None
        objectMask=None
        
        if Alpha2WhitePNG:
            sampleImg=cv.imread(sampleImgPath, cv.IMREAD_UNCHANGED)
            alpha=sampleImg[:,:,3]
            alpha=np.repeat(np.expand_dims(alpha, axis=2), 3, axis=2);
            objectMask=np.copy(alpha)
            sampleImg=sampleImg[:,:,0:3]
            alpha=cv.multiply(sampleImg, alpha)
            background=np.full(alpha.shape, 255, dtype=alpha.dtype)
            background=cv.bitwise_not(alpha, background)
            sampleImg=cv.bitwise_and(alpha, sampleImg)
            sampleImg=cv.add(sampleImg, background)
        else:
            sampleImg=cv.imread(sampleImgPath)
            
        sampleImg=cropImage(sampleImg, objectMask, CannyLowThreshold)
        #cv.imshow('dsfsdfs', sampleImg)

        dimensions=np.shape(sampleImg)

        count=0
        lower = np.array([bgColor - bgThresh, bgColor - bgThresh, bgColor - bgThresh])
        upper = np.array([bgColor + bgThresh, bgColor + bgThresh, bgColor + bgThresh])
        ImgModifier=SampImgModifier(sampleImg,dimensions,lower,upper,bgColor)

        while(count<outputPerSample):

            bkgImg=bkgFileLoader.bkgImgList[np.random.randint(0,bkgFileLoader.count)]
            GaussianNoiseFlag  = np.less(np.random.uniform(0, 1),GaussianNoiseProb)
            MedianNoiseFlag    = np.less(np.random.uniform(0, 1),MedianNoiseProb)
            SharpenFlag        = np.less(np.random.uniform(0, 1),SharpenProb)
            PersTransFlag      = np.less(np.random.uniform(0, 1),PerspTransProb)
            ScalingFlag        = np.less(np.random.uniform(0, 1), ScalingProb)
            BrightnessFlag     = np.less(np.random.uniform(0, 1), BrightnesProb)
            AffineRotateFlag   = np.less(np.random.uniform(0, 1), AffineRotateProb)

            if (PersTransFlag):
                ImgModifier.perspectiveTransform(maxXangle_Persp,maxYangle_Persp,maxZangle_Persp,bgColor)

            if (AffineRotateFlag and not PersTransFlag):
                ImgModifier.affineRotate(maxAngle_Affine,bgColor)

            if(GaussianNoiseFlag):
                ImgModifier.addGaussianNoise(0,2)

            if(MedianNoiseFlag and not GaussianNoiseFlag ):
                percentPixels=0.02
                percentSalt=0.5
                ImgModifier.addMedianNoise(percentPixels,percentSalt)

            if(SharpenFlag and not(MedianNoiseFlag) and not (GaussianNoiseFlag)):
                ImgModifier.sharpenImage()

            if (ScalingFlag):
                scale=np.random.uniform(0.5,1.5)
                ImgModifier.scaleImage(scale)

            if(BrightnessFlag and not(SharpenFlag) and not(MedianNoiseFlag) and not (GaussianNoiseFlag)):
                scale = np.random.uniform(0.5, 1)
                ImgModifier.modifybrightness(scale)

            ImgModifier.foregroundPix = (np.where(ImgModifier.maskImage == 0))
            if len(ImgModifier.foregroundPix[0]) == 0 or len(ImgModifier.foregroundPix[1]) == 0:
                print("Continue...")
                ImgModifier.resetFlags()
                continue
                
            foregroundPixTight, outImgTight,BoundRect = ImgModifier.getTightBoundbox()

            flag,finalImg,finalBoundRect= placeDistortedSample(outImgTight,foregroundPixTight,BoundRect, bkgImg)
            if(flag==True):
                outputName= filename + '_'+ str(count)
                cv.imwrite(os.path.join(outputfolder,str(outputName + '.jpg')),finalImg)
                with open(os.path.join(outputfolder,str(outputName + '.txt')),'w') as f:
                    details='0 '+' '.join(str(coord) for coord in np.reshape(finalBoundRect,4))+'\n'
                    f.write(details)
                count=count+1
            else:
                outputName = filename + '_' + str(count)
                cv.imwrite(os.path.join(outputfolder, str(outputName + '.jpg')), ImgModifier.modifiedImg)
                #cv.imshow("modified",ImgModifier.modifiedImg)
                #cv.waitKey(100)

            ImgModifier.resetFlags()

if __name__ == '__main__':
    main()